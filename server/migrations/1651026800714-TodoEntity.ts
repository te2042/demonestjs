// import {MigrationInterface, QueryRunner} from "typeorm";

// export class TodoEntity1651026800714 implements MigrationInterface {
//     name = 'TodoEntity1651026800714'

//     public async up(queryRunner: QueryRunner): Promise<void> {
//         await queryRunner.query(`DROP INDEX \`IDX_78a916df40e02a9deb1c4b75ed\` ON \`user\``);
//         await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`username\` \`userName\` varchar(255) NOT NULL`);
//         await queryRunner.query(`CREATE TABLE \`todo\` (\`id\` varchar(36) NOT NULL, \`name\` varchar(255) NOT NULL, \`description\` varchar(255) NULL, \`createOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
//         await queryRunner.query(`CREATE TABLE \`task\` (\`id\` varchar(36) NOT NULL, \`name\` varchar(255) NOT NULL, \`createOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`todoId\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
//         await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`userName\``);
//         await queryRunner.query(`ALTER TABLE \`user\` ADD \`userName\` varchar(255) NOT NULL`);
//         await queryRunner.query(`ALTER TABLE \`user\` ADD UNIQUE INDEX \`IDX_da5934070b5f2726ebfd3122c8\` (\`userName\`)`);
//         await queryRunner.query(`ALTER TABLE \`task\` ADD CONSTRAINT \`FK_91440d017e7b30d2ac16a27d762\` FOREIGN KEY (\`todoId\`) REFERENCES \`todo\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
//     }

//     public async down(queryRunner: QueryRunner): Promise<void> {
//         await queryRunner.query(`ALTER TABLE \`task\` DROP FOREIGN KEY \`FK_91440d017e7b30d2ac16a27d762\``);
//         await queryRunner.query(`ALTER TABLE \`user\` DROP INDEX \`IDX_da5934070b5f2726ebfd3122c8\``);
//         await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`userName\``);
//         await queryRunner.query(`ALTER TABLE \`user\` ADD \`userName\` varchar(255) NOT NULL`);
//         await queryRunner.query(`DROP TABLE \`task\``);
//         await queryRunner.query(`DROP TABLE \`todo\``);
//         await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`userName\` \`username\` varchar(255) NOT NULL`);
//         await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_78a916df40e02a9deb1c4b75ed\` ON \`user\` (\`username\`)`);
//     }

// }
