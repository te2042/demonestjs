import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { TaskEntity } from "./task.entity";

@Entity('todo')
export class TodoEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  
  @Column({
    type: "nvarchar"
  })
  name: string;

  @Column({
    type: 'nvarchar',
    nullable: true
  })
  description?: string;

  @CreateDateColumn()
  createOn?: Date;

  @UpdateDateColumn()
  updateOn?: Date;

  // 1 todo has 1 or n task
  @OneToMany(type => TaskEntity, task => task.todo)
  tasks: TaskEntity[];
}
