import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TodoEntity } from "./todo.entity";

@Entity('task')
export class TaskEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type:"nvarchar",
  })
  name: string;

  @CreateDateColumn()
  createOn?: Date;

  // many task for 1 todo
  @ManyToOne(type => TodoEntity, todo => todo.tasks)
  todo?:TodoEntity;
}
