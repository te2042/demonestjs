import { TodoCreateDto } from "./create-todo.dto";

export class TodoUpdateDto extends TodoCreateDto {}
