import { IsArray, IsNotEmpty } from "class-validator";
import { TaskDto } from "../taskDto/task.dto";

export class TodoDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  name: string;

  createdOn?: Date;
  description?: string;

  @IsArray()
  tasks?: TaskDto[];
}
