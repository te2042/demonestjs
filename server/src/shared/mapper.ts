import { TaskDto } from "src/dtos/taskDto/task.dto";
import { TodoDto } from "src/dtos/todoDto/todo.dto";
import { UserDto } from "src/dtos/users/user.dto";
import { TaskEntity } from "src/entities/task.entity";
import { TodoEntity } from "src/entities/todo.entity";
import { User } from "src/entities/user.entity";

// This is a utility function that maps a TodoEntity into a TodoDto
// export const toTodoDto = (data: TodoEntity): TodoDto => {
//   const { id, name, description } = data;
//   let todoDto: TodoDto = { id, name, description };
//   return todoDto;
// };

export const toTodoDto = (data: TodoEntity): TodoDto => {
  const { id, name, description, tasks } = data;
  let todoDto: TodoDto = { id, name, description };

  if(tasks) {
    todoDto = {
      ...todoDto,
      tasks: tasks.map((task: TaskEntity) => toTaskDto(task)) 
    };
  }

  return todoDto;
}

export const toTaskDto = (data: TaskEntity): TaskDto => {
  const { id, name } = data;
  let taskDto: TaskDto = { id, name };
  return taskDto;
}

export const toUserDto = (data: User): UserDto => {
  const { id, userName, email } = data;
  let userDto: UserDto = { id, userName, email };
  return userDto;
}
