import * as bcrypt from 'bcrypt';

// This is a utility function that maps a TodoEntity into a TodoDto
export const toPromise  = <T>(data: T): Promise<T> => {
  return new Promise<T>(resolve => { resolve(data) });
};

export const comparePassword = async (userPassword, inputPassword) => {
  return await bcrypt.compare(inputPassword, userPassword);
};
