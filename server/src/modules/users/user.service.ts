import { Injectable, NotFoundException } from '@nestjs/common';
import { compare } from 'bcrypt';
import { CreateUserDto } from 'src/dtos/users/create-user.dto';
import { LoginUserDto } from 'src/dtos/users/login-user.dto';
import { UserDto } from 'src/dtos/users/user.dto';
import { User } from 'src/entities/user.entity';
import { UserRepository } from 'src/repositories/user.repository';
import { toUserDto } from 'src/shared/mapper';
import { comparePassword } from 'src/shared/utils';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async findOneUser({ userName }: any): Promise<UserDto> {
    const user = await this.userRepository.findOne({ where: { userName } });
    if (!user) {
      throw new NotFoundException();
    }

    return toUserDto(user);
  }

  async findByLoginUser({ userName, password }: LoginUserDto): Promise<UserDto> {
    const user = await this.userRepository.findOne({ where: { userName } });
    if(!user) 
      throw new NotFoundException('User not found');
    
    // compare password
    const comparePass = await comparePassword(user.password, password);
    if(!comparePass) 
      throw new NotFoundException('Password is wrong!!!');
    
    return toUserDto(user);
  }

  async create(userDto: CreateUserDto): Promise<UserDto> {
    const { userName, password, email } = userDto;
    const userInDb = await this.userRepository.findOne({ where: { userName } });
    if(userInDb) 
      throw new NotFoundException(" User already exist");
    
    const newUser: User = await this.userRepository.create({
      userName,
      password,
      email
    });
    await this.userRepository.save(newUser);
    return toUserDto(newUser);
  }
}
