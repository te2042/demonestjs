import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/dtos/users/create-user.dto';
import { LoginUserDto } from 'src/dtos/users/login-user.dto';
import { UserDto } from 'src/dtos/users/user.dto';
import { UserService } from '../users/user.service';
import { LoginStatus } from './interface/login-status.interface';
import { JwtPayload } from './interface/payload.interface';
import { RegistrationStatus } from './interface/regisration-status.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async register(userDto: CreateUserDto): Promise<RegistrationStatus> {
    let status: RegistrationStatus = {
      success: true,
      message: 'user registered'
    };

    try {
      await this.usersService.create(userDto);
    } catch (err) {
      status = {
        success: false,
        message: err
      };
    }

    return status;
  }

  async login(loginUserDto: LoginUserDto): Promise<LoginStatus> {
    // find user in db
    const user = await this.usersService.findByLoginUser(loginUserDto);

    // generate and sign token
    const token = this._createToken(user);

    return {
      username: user.userName,
      ...token,
    };
  }

  async validateUser(payload: JwtPayload): Promise<UserDto> {
    const user = await this.usersService.findOneUser(payload);
    if (!user) {
      throw new NotFoundException('Invalid token');
    }
    return user;
  }

  private _createToken({ userName }: UserDto): any {
    const user: JwtPayload = { userName };
    const accessToken = this.jwtService.sign(user);
    return {
      accessToken,
    };
  }
}
