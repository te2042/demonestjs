import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { TodoCreateDto } from 'src/dtos/todoDto/create-todo.dto';
import { TodoListDto } from 'src/dtos/todoDto/list-todo.dto';
import { TodoDto } from 'src/dtos/todoDto/todo.dto';
import { TodoUpdateDto } from 'src/dtos/todoDto/update-todo.dto';
import { TodoService } from './todo.service';

@Controller('todos')
export class TodoController {
  constructor(private readonly todoSevice: TodoService) {}

  @Get()
  async findAll(@Req() req: any): Promise<TodoListDto> {
    const todos = await this.todoSevice.getAllTodo();
    return { todos };
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<TodoDto> {
    return await this.todoSevice.getOneTodo(id);
  }

  @Post()
  async create(
    @Body() createTodoDto: TodoCreateDto,
  ): Promise<TodoDto> {
    return await this.todoSevice.createDto(createTodoDto);
  }

  @Put('id')
  async update(
    @Param('id') id: string,
    @Body() body: TodoUpdateDto
  ): Promise<TodoDto> {
    return await this.todoSevice.updateTodo(id, body);
  }

  @Delete('id')
  async delete(@Param('id') id: string): Promise<string> {
    return await this.todoSevice.deleteTodo(id);
  }
}

