import { Injectable, NotFoundException } from '@nestjs/common';
import { TodoCreateDto } from 'src/dtos/todoDto/create-todo.dto';
import { TodoDto } from 'src/dtos/todoDto/todo.dto';
import { TodoUpdateDto } from 'src/dtos/todoDto/update-todo.dto';
import { TodoEntity } from 'src/entities/todo.entity';
import { TodoRepository } from 'src/repositories/todo.repository';
import { toTodoDto } from 'src/shared/mapper';

@Injectable()
export class TodoService {
  constructor(private readonly todoRepository: TodoRepository) {}

  async getAllTodo(): Promise<TodoDto[]> {
    const todos = await this.todoRepository.find();
    return todos.map(todo => toTodoDto(todo));
  }

  async getOneTodo(id: string): Promise<TodoDto> {
    const todo = await this.todoRepository.findOne(id);
    if(!todo)
      throw new NotFoundException('Todo not found');
    
    return toTodoDto(todo);
  }

  async createDto(createTodoDto: TodoCreateDto): Promise<TodoDto> {
    const todo: TodoEntity = await this.todoRepository.create(createTodoDto);
    await this.todoRepository.save(todo);
    return toTodoDto(todo);
  }

  async updateTodo(id: string, updateDto: TodoUpdateDto): Promise<TodoEntity> {
    const oldTodo = await this.getOneTodo(id);
    const newTodo = {...oldTodo, updateDto};
    return await this.todoRepository.save(newTodo);
  }

  async deleteTodo(id: string): Promise<string> {
    const todo = await this.todoRepository.softDelete(id);
    if(!todo.affected)
      throw new NotFoundException('Todo not found');
    
    return "Todo has been deleted";
  }
}
