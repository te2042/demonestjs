import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TodoRepository } from 'src/repositories/todo.repository';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../users/user.module';
import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';

@Module({
  imports: [
    UserModule,
    AuthModule,
    TypeOrmModule.forFeature([TodoRepository])],
  controllers: [TodoController],
  providers: [TodoService]
})
export class TodoModule {}
