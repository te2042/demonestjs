"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
exports.default = [
    {
        type: 'mysql',
        host: process.env.DB_HOST,
        port: Number.parseInt(process.env.DB_PORT),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: [__dirname + '/src/**/**.entity{.ts,.js}'],
        migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
        migrationsRun: false,
        cli: {
            entitiesDir: __dirname + '/src/entities',
            migrationsDir: __dirname + '/migrations',
        },
        timezone: 'Z',
        synchronize: false,
        debug: process.env.NODE_ENV === 'development' ? true : false,
    },
];
//# sourceMappingURL=ormconfig.js.map