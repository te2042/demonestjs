import { TaskEntity } from "./task.entity";
export declare class TodoEntity {
    id: string;
    name: string;
    description?: string;
    createOn?: Date;
    updateOn?: Date;
    tasks: TaskEntity[];
}
