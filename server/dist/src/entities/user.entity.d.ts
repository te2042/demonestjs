export declare class User {
    id: string;
    userName: string;
    password: string;
    email: string;
    hashPassword(): Promise<void>;
}
