import { TodoEntity } from "./todo.entity";
export declare class TaskEntity {
    id: string;
    name: string;
    createOn?: Date;
    todo?: TodoEntity;
}
