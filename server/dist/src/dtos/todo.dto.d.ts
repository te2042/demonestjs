export declare class TodoDto {
    id: string;
    name: string;
    createdOn?: Date;
    description?: string;
}
