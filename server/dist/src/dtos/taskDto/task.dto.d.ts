export declare class TaskDto {
    id: string;
    name: string;
    createdOn?: Date;
}
