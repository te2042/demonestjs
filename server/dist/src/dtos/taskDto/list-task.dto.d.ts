import { TaskDto } from './task.dto';
export declare class TaskListDto {
    tasks: TaskDto[];
}
