import { TaskDto } from "../taskDto/task.dto";
export declare class TodoDto {
    id: string;
    name: string;
    createdOn?: Date;
    description?: string;
    tasks?: TaskDto[];
}
