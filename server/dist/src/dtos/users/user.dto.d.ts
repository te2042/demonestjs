export declare class UserDto {
    id: string;
    userName: string;
    email: string;
}
