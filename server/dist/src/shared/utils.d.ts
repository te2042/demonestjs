export declare const toPromise: <T>(data: T) => Promise<T>;
export declare const comparePassword: (userPassword: any, inputPassword: any) => Promise<boolean>;
