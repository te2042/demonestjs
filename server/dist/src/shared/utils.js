"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.comparePassword = exports.toPromise = void 0;
const bcrypt = require("bcrypt");
const toPromise = (data) => {
    return new Promise(resolve => { resolve(data); });
};
exports.toPromise = toPromise;
const comparePassword = async (userPassword, inputPassword) => {
    return await bcrypt.compare(inputPassword, userPassword);
};
exports.comparePassword = comparePassword;
//# sourceMappingURL=utils.js.map