"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toUserDto = exports.toTaskDto = exports.toTodoDto = void 0;
const toTodoDto = (data) => {
    const { id, name, description, tasks } = data;
    let todoDto = { id, name, description };
    if (tasks) {
        todoDto = Object.assign(Object.assign({}, todoDto), { tasks: tasks.map((task) => (0, exports.toTaskDto)(task)) });
    }
    return todoDto;
};
exports.toTodoDto = toTodoDto;
const toTaskDto = (data) => {
    const { id, name } = data;
    let taskDto = { id, name };
    return taskDto;
};
exports.toTaskDto = toTaskDto;
const toUserDto = (data) => {
    const { id, userName, email } = data;
    let userDto = { id, userName, email };
    return userDto;
};
exports.toUserDto = toUserDto;
//# sourceMappingURL=mapper.js.map