import { TaskDto } from "src/dtos/taskDto/task.dto";
import { TodoDto } from "src/dtos/todoDto/todo.dto";
import { UserDto } from "src/dtos/users/user.dto";
import { TaskEntity } from "src/entities/task.entity";
import { TodoEntity } from "src/entities/todo.entity";
import { User } from "src/entities/user.entity";
export declare const toTodoDto: (data: TodoEntity) => TodoDto;
export declare const toTaskDto: (data: TaskEntity) => TaskDto;
export declare const toUserDto: (data: User) => UserDto;
