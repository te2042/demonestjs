import { TodoCreateDto } from 'src/dtos/todoDto/create-todo.dto';
import { TodoListDto } from 'src/dtos/todoDto/list-todo.dto';
import { TodoDto } from 'src/dtos/todoDto/todo.dto';
import { TodoUpdateDto } from 'src/dtos/todoDto/update-todo.dto';
import { TodoService } from './todo.service';
export declare class TodoController {
    private readonly todoSevice;
    constructor(todoSevice: TodoService);
    findAll(req: any): Promise<TodoListDto>;
    findOne(id: string): Promise<TodoDto>;
    create(createTodoDto: TodoCreateDto): Promise<TodoDto>;
    update(id: string, body: TodoUpdateDto): Promise<TodoDto>;
    delete(id: string): Promise<string>;
}
