import { TodoCreateDto } from 'src/dtos/todoDto/create-todo.dto';
import { TodoDto } from 'src/dtos/todoDto/todo.dto';
import { TodoUpdateDto } from 'src/dtos/todoDto/update-todo.dto';
import { TodoEntity } from 'src/entities/todo.entity';
import { TodoRepository } from 'src/repositories/todo.repository';
export declare class TodoService {
    private readonly todoRepository;
    constructor(todoRepository: TodoRepository);
    getAllTodo(): Promise<TodoDto[]>;
    getOneTodo(id: string): Promise<TodoDto>;
    createDto(createTodoDto: TodoCreateDto): Promise<TodoDto>;
    updateTodo(id: string, updateDto: TodoUpdateDto): Promise<TodoEntity>;
    deleteTodo(id: string): Promise<string>;
}
