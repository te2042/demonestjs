"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoService = void 0;
const common_1 = require("@nestjs/common");
const todo_repository_1 = require("../../repositories/todo.repository");
const mapper_1 = require("../../shared/mapper");
let TodoService = class TodoService {
    constructor(todoRepository) {
        this.todoRepository = todoRepository;
    }
    async getAllTodo() {
        const todos = await this.todoRepository.find();
        return todos.map(todo => (0, mapper_1.toTodoDto)(todo));
    }
    async getOneTodo(id) {
        const todo = await this.todoRepository.findOne(id);
        if (!todo)
            throw new common_1.NotFoundException('Todo not found');
        return (0, mapper_1.toTodoDto)(todo);
    }
    async createDto(createTodoDto) {
        const todo = await this.todoRepository.create(createTodoDto);
        await this.todoRepository.save(todo);
        return (0, mapper_1.toTodoDto)(todo);
    }
    async updateTodo(id, updateDto) {
        const oldTodo = await this.getOneTodo(id);
        const newTodo = Object.assign(Object.assign({}, oldTodo), { updateDto });
        return await this.todoRepository.save(newTodo);
    }
    async deleteTodo(id) {
        const todo = await this.todoRepository.softDelete(id);
        if (!todo.affected)
            throw new common_1.NotFoundException('Todo not found');
        return "Todo has been deleted";
    }
};
TodoService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [todo_repository_1.TodoRepository])
], TodoService);
exports.TodoService = TodoService;
//# sourceMappingURL=todo.service.js.map