"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const user_repository_1 = require("../../repositories/user.repository");
const mapper_1 = require("../../shared/mapper");
const utils_1 = require("../../shared/utils");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async findOneUser({ userName }) {
        const user = await this.userRepository.findOne({ where: { userName } });
        if (!user) {
            throw new common_1.NotFoundException();
        }
        return (0, mapper_1.toUserDto)(user);
    }
    async findByLoginUser({ userName, password }) {
        const user = await this.userRepository.findOne({ where: { userName } });
        if (!user)
            throw new common_1.NotFoundException('User not found');
        const comparePass = await (0, utils_1.comparePassword)(user.password, password);
        if (!comparePass)
            throw new common_1.NotFoundException('Password is wrong!!!');
        return (0, mapper_1.toUserDto)(user);
    }
    async create(userDto) {
        const { userName, password, email } = userDto;
        const userInDb = await this.userRepository.findOne({ where: { userName } });
        if (userInDb)
            throw new common_1.NotFoundException(" User already exist");
        const newUser = await this.userRepository.create({
            userName,
            password,
            email
        });
        await this.userRepository.save(newUser);
        return (0, mapper_1.toUserDto)(newUser);
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map