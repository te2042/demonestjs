import { CreateUserDto } from 'src/dtos/users/create-user.dto';
import { LoginUserDto } from 'src/dtos/users/login-user.dto';
import { UserDto } from 'src/dtos/users/user.dto';
import { UserRepository } from 'src/repositories/user.repository';
export declare class UserService {
    private readonly userRepository;
    constructor(userRepository: UserRepository);
    findOneUser({ userName }: any): Promise<UserDto>;
    findByLoginUser({ userName, password }: LoginUserDto): Promise<UserDto>;
    create(userDto: CreateUserDto): Promise<UserDto>;
}
