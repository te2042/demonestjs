import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/dtos/users/create-user.dto';
import { LoginUserDto } from 'src/dtos/users/login-user.dto';
import { UserDto } from 'src/dtos/users/user.dto';
import { UserService } from '../users/user.service';
import { LoginStatus } from './interface/login-status.interface';
import { JwtPayload } from './interface/payload.interface';
import { RegistrationStatus } from './interface/regisration-status.interface';
export declare class AuthService {
    private readonly usersService;
    private readonly jwtService;
    constructor(usersService: UserService, jwtService: JwtService);
    register(userDto: CreateUserDto): Promise<RegistrationStatus>;
    login(loginUserDto: LoginUserDto): Promise<LoginStatus>;
    validateUser(payload: JwtPayload): Promise<UserDto>;
    private _createToken;
}
