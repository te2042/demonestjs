declare const _default: {
    type: string;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    entities: string[];
    migrations: string[];
    migrationsRun: boolean;
    cli: {
        entitiesDir: string;
        migrationsDir: string;
    };
    timezone: string;
    synchronize: boolean;
    debug: boolean;
}[];
export default _default;
