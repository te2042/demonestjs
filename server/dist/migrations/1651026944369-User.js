"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User1651026944369 = void 0;
class User1651026944369 {
    constructor() {
        this.name = 'User1651026944369';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE \`user\` (\`id\` varchar(36) NOT NULL, \`userName\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`email\` varchar(255) NOT NULL, UNIQUE INDEX \`IDX_da5934070b5f2726ebfd3122c8\` (\`userName\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP INDEX \`IDX_da5934070b5f2726ebfd3122c8\` ON \`user\``);
        await queryRunner.query(`DROP TABLE \`user\``);
    }
}
exports.User1651026944369 = User1651026944369;
//# sourceMappingURL=1651026944369-User.js.map