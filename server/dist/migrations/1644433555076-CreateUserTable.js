"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserTable1644433555076 = void 0;
class CreateUserTable1644433555076 {
    constructor() {
        this.name = 'CreateUserTable1644433555076';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE \`users\` (\`id\` int NOT NULL AUTO_INCREMENT, \`first_name\` varchar(255) NOT NULL, \`last_name\` varchar(255) NOT NULL, \`is_active\` tinyint NOT NULL DEFAULT '1', \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE \`users\``);
    }
}
exports.CreateUserTable1644433555076 = CreateUserTable1644433555076;
//# sourceMappingURL=1644433555076-CreateUserTable.js.map